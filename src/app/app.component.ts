import { Component, OnInit } from '@angular/core';
import {MatDialog, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { DialogComponent } from './dialog/dialog.component';
import { ApiService } from './services/api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'ang-13-project';
   constructor(private dialog : MatDialog, private api: ApiService){

   }
  ngOnInit(): void {
    this.getAllUsers();
  }
  openDialog() {
    this.dialog.open(DialogComponent, {
        width: "30%"
    });
  }
  getAllUsers(){
    this.api.getUser()
    .subscribe({
      next : (res)=> {
        console.log(res);    
      },
      error: (err)=>{
        console.log(err)
      }
    })
  }
}
