import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from '../services/api.service';
import { MatDialogRef } from '@angular/material/dialog';
@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css']
})
export class DialogComponent implements OnInit {
     
   signupForm !: FormGroup;

  constructor(private formBuilder: FormBuilder, private api : ApiService, private dialogRef: MatDialogRef<DialogComponent>) { }

  ngOnInit(): void {
     this.signupForm = this.formBuilder.group({
       Name: ['', Validators.required],
       Gender: ['', Validators.required],
       DateOfBirth: ['', Validators.required],
       City: ['', Validators.required]
     })
  }
  signupUser(){
    console.log(this.signupForm.value);
    if(this.signupForm.valid){
      this.api.postUser(this.signupForm.value)
      .subscribe({
         next:(res)=> {
           console.log("User Added Successfully!"); 
           this.signupForm.reset();   
           this.dialogRef.close();
         },
         error: ()=>{
           console.log("Error in SignUp");
           
         }
      })
    }
    
  }

}
